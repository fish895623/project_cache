import re


class IgnoreParser:
    def __init__(self) -> None:
        pass


class GitIgnoreParser(IgnoreParser):
    def __init__(self) -> None:
        super().__init__()


class Ignore:
    def __init__(self, re: bool = True) -> None:
        pass
