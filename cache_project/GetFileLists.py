import os
import hashlib


class WalkFileDir:
    def __init__(self) -> None:
        pass

    def ww(self):
        for root, dir, files in os.walk(".", topdown=False):
            print(root, dir, files)

    # def hash_file(self) -> None:
    #     with open("./tests/__init__.py", "rb") as file:
    #         hash = hashlib.sha1()
    #         hash.update(file.read(1024))
    #         print(hash.hexdigest())


WalkFileDir().ww()
